from django.shortcuts import render
from django.http import HttpResponse
from .models import querycommand
# Create your views here.
def home(request):
    my_dict={
        'A':"Hello Nikhil",
        'B':"ERK"
    }
    return render(request,'index.html',context=my_dict)

def dataDisplay(request):
    qrcommand=querycommand.objects.all()
    my_dict={
        'data':qrcommand

    }
    return render(request,'querycommand.html',context=my_dict)
