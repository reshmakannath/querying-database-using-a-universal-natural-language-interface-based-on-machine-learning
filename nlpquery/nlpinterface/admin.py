from django.contrib import admin
from.models import querycommand,NLTcommand,signingtable,signwords,aggregatecommand


# Register your models here.

class querycommandadmin(admin.ModelAdmin):
    list_display=('command',)
    search_fields=('command',)

admin.site.register(querycommand,querycommandadmin)

class NLTcommandadmin(admin.ModelAdmin):
    list_display=('NLcommand','queryid')
    search_fields=('NLcommand','queryid')

admin.site.register(NLTcommand,NLTcommandadmin)

class signingtableadmin(admin.ModelAdmin):
    list_display=('signs',)
    search_fields=('signs',)

admin.site.register(signingtable,signingtableadmin)


class signwordsadmin(admin.ModelAdmin):
    list_display=('sid','suserdata')
    search_fields=('sid','suserdata')

admin.site.register(signwords,signwordsadmin)


class aggregatecommandadmin(admin.ModelAdmin):
    list_display=('qid','agcommand')
    search_fields=('qid','agcommand')

admin.site.register(aggregatecommand,aggregatecommandadmin)
