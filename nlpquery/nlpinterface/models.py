from django.db import models

# Create your models here.
class querycommand(models.Model):
    command=models.CharField(max_length=50)
    def __str__(self):
        return self.command

class NLTcommand(models.Model):
    NLcommand=models.CharField(max_length=50)
    queryid=models.ForeignKey(querycommand,on_delete=models.CASCADE)
    def __str__(self):
        return self.NLcommand

class signingtable(models.Model):
    signs=models.CharField(max_length=50)
    def __str__(self):
        return self.signs

class signwords(models.Model):
    sid=models.ForeignKey(signingtable,on_delete=models.CASCADE)
    suserdata=models.CharField(max_length=50)
    def __str__(self):
        return self.sid

class aggregatecommand(models.Model):
    qid=models.ForeignKey(querycommand,on_delete=models.CASCADE)
    agcommand=models.CharField(max_length=50)
    def __str__(self):
        return self.qid