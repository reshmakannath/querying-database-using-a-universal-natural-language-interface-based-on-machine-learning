from django.apps import AppConfig


class NlpinterfaceConfig(AppConfig):
    name = 'nlpinterface'
