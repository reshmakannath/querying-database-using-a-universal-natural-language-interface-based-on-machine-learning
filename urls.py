from django.contrib import admin
from django.conf.urls import url,include
from . import views

app_name="nlpsql"

urlpatterns = [
    
     url(r'^$', views.index,name='index'),
      url(r'^querycmd/', views.querycmd,name='querycmd'),
      url(r'^remove/(?P<cmd_id>[0-9]+)/$', views.cmdremove,name='cmdremove'),
      url(r'^nlcmd/', views.nlcmd,name='nlcmd'),
      url(r'^nlremove/(?P<cmd_id>[0-9]+)/$', views.nlremove,name='nlremove'),
]
